from django.urls import path
from receipts.views import (
    list_receipts,
    create_receipt,
    ExpenseCategory_list,
    Account_list,
    create_category,
    create_account,
)

urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", ExpenseCategory_list, name="category_list"),
    path("accounts/", Account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
